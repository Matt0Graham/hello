package com.citi.training.product;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class InMemoryProductRepositoryTests {

	@Test
	public void testSaveProduct() {
		InMemoryProductRepository pRepo = new InMemoryProductRepository();
		Product p1 = new Product(1,"test",40.00);
		pRepo.saveProduct(p1);
		assert(pRepo.getProduct(0).equals(p1));
	}
//
//	@Test
//	public void testGetProduct() {
//		fail("Not yet implemented");
//	}

	@Test
	public void testGetAllProducts() {
	
		InMemoryProductRepository pRepo = new InMemoryProductRepository();
		List<Product> pList = new ArrayList<Product>();
		assert(pRepo.getAllProducts().equals(pList));
	}

}

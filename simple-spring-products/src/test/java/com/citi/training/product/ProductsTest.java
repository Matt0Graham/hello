package com.citi.training.product;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProductsTest {

//	
//	
//	
//	//public void setName(String name) {
//	this.name = name;
//}

//
//public void setPrice(double price) {
//	this.price = price;
//}


	
	
	@Test
	public void test() {
		double price = 60.00;
		Product p1 = new Product(1,"JamesDonuts",50.00);
		p1.setPrice(price);
		assert(p1.getPrice()==price);
	}
	

	@Test
	public void testName() {
		String name = "balls";
		Product p1 = new Product(1,"JamesDonuts",50.00);
		p1.setName(name);
		assert(p1.getName()==name);
	}

	@Test
	public void testId() {
		int id = 1;
		Product p1 = new Product(1,"JamesDonuts",50.00);
		p1.setId(id);
		assert(p1.getId()==id);
	}
	
	
	
	

}

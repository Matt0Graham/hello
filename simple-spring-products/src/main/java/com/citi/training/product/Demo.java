package com.citi.training.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component 
@Profile(value = { "demo" })
public class Demo {

	@Autowired
	public void run(ApplicationArguments appArgs) {
		Product p1 = new Product(1,"donuts",40.00);
		Product p2 = new Product(2,"apples",20.00);
		Product p3 = new Product(3,"oranges",30.00);
		Product p4 = new Product(4,"bannana",20.00);
		
		InMemoryProductRepository pRepo = new InMemoryProductRepository();
		pRepo.saveProduct(p1);
		pRepo.saveProduct(p2);
		pRepo.saveProduct(p3);
		pRepo.saveProduct(p4);
		
		for(Product p : pRepo.getAllProducts()) {
			System.out.println(p.toString());
		}
		
	}

}

/**
 * 
 */
package com.citi.training.product;

/**
 * @author Administrator
 *
 */
public class Product {
	
	private int id;
	private String name;
	private double price;
	
	Product(){
		
	}
	
	Product(int id, String name, double price){
		this.setId(id);
		this.setName(name);
		this.setPrice(price);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		String string = "ID: "+this.getId() + " Name: " +this.getName() 
		+ " Price: "+this.getPrice();
		return string;
	}
	
	
	
}

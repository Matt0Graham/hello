package com.citi.training.product;


import java.util.ArrayList;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class InMemoryProductRepository implements ProductRepository {

	List<Product> pList = new ArrayList<Product>();
	
	
	@Override
	public void saveProduct(Product product) {
		pList.add(product);
	}

	@Override
	public Product getProduct(int id) {
	
		return pList.get(id);
	}

	@Override
	public List<Product> getAllProducts() {
		// TODO Auto-generated method stub
		return this.pList;
	}

}

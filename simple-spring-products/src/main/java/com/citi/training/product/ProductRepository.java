package com.citi.training.product;

import java.util.List;

public interface ProductRepository {

	void saveProduct(Product product);
	Product getProduct(int id);
	List<Product> getAllProducts();
}

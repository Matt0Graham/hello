package com.citi.training.product;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile(value = { "default" })
public class Menu {

	@Autowired
	public void run(ApplicationArguments appArgs) {
		int option = -1;
		InMemoryProductRepository pRepo = new InMemoryProductRepository();
		Scanner keyb = new Scanner(System.in);

		do {
			System.out.println("--------Product Menu------");
			System.out
					.println("Enter 1 to add a new product" + "\nEnter 2 to get a product by ID\nEnter 3 to get all products");
			option = keyb.nextInt();

			switch (option) {
			case 1:
				System.out.println("Please enter a product name");
				String name = keyb.next();

				System.out.println("Please enter an ID");
				int id = keyb.nextInt();

				System.out.println("Please enter a price");
				double price = keyb.nextDouble();

				pRepo.saveProduct(new Product(id, name, price));

				break;
			case 2:
				System.out.println("Please enter the id of a product");
				System.out.println(pRepo.getProduct((keyb.nextInt())-1).toString());
				break;
			case 3:
				System.out.println("Getting all products");
				System.out.println(pRepo.getAllProducts());

				break;

			default:
				System.out.println("Erroneuos input");
			}

		} while (option != -1);

	}

}

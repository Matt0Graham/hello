package com.citi.training.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleSpringProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleSpringProductsApplication.class, args);
	}
}

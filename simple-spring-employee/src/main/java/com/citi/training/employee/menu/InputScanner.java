package com.citi.training.employee.menu;

import java.util.Scanner;

public interface InputScanner {

	// Get a String from the user.
	String getString(String promptMsg);

	// Get a double from the user.
	double getDouble(String promptMsg);

	// Get an int from the user.
	int getInt(String promptMsg);

	void clearScanner();
}

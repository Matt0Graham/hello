package com.citi.training.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleSpringEmployeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleSpringEmployeeApplication.class, args);
	}
}

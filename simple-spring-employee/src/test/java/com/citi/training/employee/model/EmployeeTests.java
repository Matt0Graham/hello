package com.citi.training.employee.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmployeeTests {

	@Test
	public void test() {
		Employee e1 = new Employee("mary");
		double startingSalary = e1.getSalary();
		double raiseAmount = 400.00;
		e1.payRaise(400.00);
		assert (startingSalary + raiseAmount == e1.getSalary());
	}
	
	@Test
	public void testDefaultPayBonus() {
		Employee e1 = new Employee("mary");
		double startingSalary = e1.getSalary();
		e1.payBonus();
		startingSalary *= 1.01;
		assert (startingSalary == e1.getSalary());
	}
	
	@Test
	public void testPayBonusPercentage() {
		Employee e1 = new Employee("mary");
		double startingSalary = e1.getSalary();
		double percentage = 0.4;	
		e1.payBonus(percentage);
		assert (startingSalary * (percentage+1)== e1.getSalary());
	}
	
	

}

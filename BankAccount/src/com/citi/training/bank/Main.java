package com.citi.training.bank;

public class Main {

	public static void main(String[] args) {

		BankAccount bank1 = new BankAccount();
		System.out.println("Account: "+bank1);
		JointAccount jbank1 = new JointAccount("steve", "harry");
		System.out.println(jbank1.toString());
	}
}

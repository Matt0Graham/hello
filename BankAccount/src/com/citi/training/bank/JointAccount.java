package com.citi.training.bank;

public class JointAccount extends BankAccount {

	private String jointAccountHolder;
	
	
	JointAccount(String accHolder1, String accHolder2){
		super(accHolder1);
		this.jointAccountHolder = accHolder2 ;
	}


	@Override
	public String toString() {
		return "JointAccount [jointAccountHolder=" + jointAccountHolder + ", bankAccount=" + super.toString() + "]";
	}
	
	
}

package com.citi.training.bank;

import java.util.Date;

public class BankAccount {
	
	private String accountHolder;
	private int id;
	private double balance =0;
	private Date creationDate = new Date();
	
	public BankAccount(){
	this("anonymous");
	System.out.println("constructor1");
	}
	
	public BankAccount(String accountHolder) {
		this.accountHolder = accountHolder;
		System.out.println("constructor2");
	}
	
	@Override
	public String toString() {
		return "BankAccount [accountHolder=" + accountHolder + ", id=" + id + ", balance=" + balance + ", creationDate="
				+ creationDate + "]";
	}

	/**
	 * @return the accountHolder
	 */
	public String getAccountHolder() {
		return accountHolder;
	}

	/**
	 * @param accountHolder the accountHolder to set
	 */
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the balance
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(double balance) {
		this.balance = balance;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	
}

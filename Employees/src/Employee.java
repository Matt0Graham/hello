import java.util.Date;

public class Employee {

	private String name;
	private double salary;
	private Date joinDate = new Date();
	
	
	Employee(String name, double salary){
		this.name = name;
		this.salary = salary;

	}
	
	Employee(String name){
		this(name,7000);
	}
	
	public void payRaise(double raise) {
		this.salary += raise;
	}
	
	public void payBonus() {
		this.salary+= (this.salary/100);
	}
	
	public void payBonus(double pRaise) {
		this.salary += this.salary/100*pRaise;
	}
	
	public void payBonus(double max, double min, double percentage) {
		
		double newSal = this.salary + percentage *100;
		
		if((newSal)<max && (newSal)> min &&  percentage < 100){
			this.salary = newSal;
		}else {
			System.out.println("percentage to big");
		}
		
	}
	
	@Override
	public String toString() {
		return "Employee [name=" + name + ", salary=" + salary + ", joinDate=" + joinDate + "]";
	}
	
}

import java.util.Date;

public class Main {

	public static void main(String[] args) {
		Employee e1 = new Employee("james",100.00);
		System.out.println(e1.toString());
		
		Employee e2 = new Employee("matt");
		System.out.println(e2.toString());
		
		e2.payBonus(12);
		System.out.println(e2.toString());
		
		e2.payBonus(18300,10000,100);
		System.out.println(e2.toString());
	}
}
